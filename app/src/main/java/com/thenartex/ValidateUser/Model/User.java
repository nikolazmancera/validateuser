package com.thenartex.ValidateUser.Model;
import android.text.TextUtils;
import android.util.Patterns;


public class User implements IUser {

    private String email,password,date,response;

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public User() {

    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getDate() {
        return date;
    }

    @Override
    public String getResponse() {
        return response;
    }

    @Override
    public void setEmail(String string) {
        this.email = string;
    }

    @Override
    public void setDate(String string) {
        this.date = string;
    }

    @Override
    public void setResponse(String string) {
        this.response = string;
    }
    @Override
    public boolean patternsMatch() {
        String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{8,20})";
        return !TextUtils.isEmpty(getEmail()) && Patterns.EMAIL_ADDRESS.matcher(getEmail()).matches() && getPassword().matches(PASSWORD_PATTERN);
    }
}
