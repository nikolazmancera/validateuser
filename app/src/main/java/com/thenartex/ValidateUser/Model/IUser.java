package com.thenartex.ValidateUser.Model;

public interface IUser {

    String getEmail();

    String getPassword();

    String getDate();

    String getResponse();

    void setEmail(String string);

    void setDate(String string);

    void setResponse(String string);

    boolean patternsMatch();
}
