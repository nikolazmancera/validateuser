package com.thenartex.ValidateUser.View;
import android.annotation.SuppressLint;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.thenartex.ValidateUser.DataBaseHelper;
import com.thenartex.ValidateUser.Presenter.ILoginPresenter;
import com.thenartex.ValidateUser.Presenter.LoginPresenter;
import com.thenartex.ValidateUser.R;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


public class Login extends Fragment implements ILoginView {

    private EditText etEmail,etPassword;
    private Button btValidate;
    private ILoginPresenter loginPresenter;
    private DataBaseHelper db;

    @SuppressLint("WrongViewCast")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_login, container, false);
        db = new DataBaseHelper(getContext());
        etEmail = (EditText) rootView.findViewById(R.id.emaillEditText);
        etPassword = (EditText) rootView.findViewById(R.id.paswordEditText);
        btValidate = (Button)rootView.findViewById(R.id.validateButton);
        loginPresenter = new LoginPresenter(this);
        btValidate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!(etEmail.getText().toString().equals("")||etPassword.getText().toString().equals("")))
                    loginPresenter.onLogin(etEmail.getText().toString(),etPassword.getText().toString());
                else
                    Toast.makeText(getContext(), "Please fill all the fields", Toast.LENGTH_SHORT).show();
            }
        });
        return rootView;
    }

    @Override
    public void onLoginResult(String message) {

        String currentDate;
        FragmentManager fragManager = this.getFragmentManager();
        boolean isValid = db.checkUser(etEmail.getText().toString(),etPassword.getText().toString());
        //Add try to DataBase
        currentDate = getLocalTime();
        db.addTry(etEmail.getText().toString(),isValid, currentDate);
            if (isValid){
                Toast.makeText(getContext(),"Valid User", Toast.LENGTH_SHORT).show();
                TryList tryListFragment = new TryList();
                fragManager.beginTransaction().replace(R.id.frameLayout,tryListFragment).commit();
            }
            else{
                Toast.makeText(getContext(),"Invalid User", Toast.LENGTH_SHORT).show();
            }
    }

    @Override
    public String getLocalTime() {

            String request = "http://api.geonames.org/timezoneJSON?formatted=true&lat=4.754267&lng=-74.046280&username=qa_mobile_easy&style=full";
            URL url = null;
            HttpURLConnection conn;
            String currentDate;

            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            try {
                url = new URL(request);
                conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.connect();
                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();
                String json = "";
                while((inputLine = in.readLine()) != null){
                    response.append(inputLine);
                }
                json = response.toString();
                JSONObject jsonObj = new JSONObject(json);
                currentDate = jsonObj.optString(("time"));

                return currentDate;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
    }

}
