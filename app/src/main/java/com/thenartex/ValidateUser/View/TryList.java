package com.thenartex.ValidateUser.View;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import androidx.fragment.app.Fragment;
import com.thenartex.ValidateUser.DataBaseHelper;
import com.thenartex.ValidateUser.Model.User;
import com.thenartex.ValidateUser.R;
import java.util.ArrayList;
import java.util.List;


public class TryList extends Fragment {

    private UserAdapter userAdapter;
    private DataBaseHelper db;
    private ListView validateList ;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_try_list, container, false);
        super.onCreate(savedInstanceState);

        db = new DataBaseHelper(getContext());
        validateList = rootView.findViewById(R.id.listview1);
        List list = db.getAllUser();
        userAdapter = new UserAdapter(getContext(), (ArrayList<User>) list);
        validateList.setAdapter(userAdapter);

        return rootView;
    }


}
