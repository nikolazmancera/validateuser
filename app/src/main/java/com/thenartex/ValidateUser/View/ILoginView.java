package com.thenartex.ValidateUser.View;

public interface ILoginView {
    void onLoginResult (String message);
    String getLocalTime();
}
