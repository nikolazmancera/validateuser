package com.thenartex.ValidateUser.View;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.thenartex.ValidateUser.DataBaseHelper;
import com.thenartex.ValidateUser.Model.User;
import com.thenartex.ValidateUser.Presenter.CreatePresenter;
import com.thenartex.ValidateUser.Presenter.ICreatePresenter;
import com.thenartex.ValidateUser.R;


public class Create extends Fragment implements ICreateView {

    private DataBaseHelper db;
    private EditText etEmail,etPassword;
    private Button btCreate;
    private ICreatePresenter createPresenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_create, container, false);
        db = new DataBaseHelper(getContext());
        etEmail = (EditText) rootView.findViewById(R.id.createEmaillEditText);
        etPassword = (EditText) rootView.findViewById(R.id.createPaswordEditText);
        btCreate = (Button)rootView.findViewById(R.id.createButton);
        createPresenter = new CreatePresenter((ICreateView) this);
        btCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createPresenter.onCreateUser(etEmail.getText().toString(),etPassword.getText().toString());
            }
        });
        return rootView;
    }

    @Override
    public void onCreateResult(String message) {

        User user = new User(etEmail.getText().toString(),etPassword.getText().toString());
        if(message.equals("OK"))
        {
            boolean isValid = db.checkUser(etEmail.getText().toString(),etPassword.getText().toString());
            if (!isValid){
                db.addUser(user);
                Toast.makeText(getContext(),"User created successfully", Toast.LENGTH_SHORT).show();
            }else
                Toast.makeText(getContext(),"This user already exist", Toast.LENGTH_SHORT).show();
        }else
            Toast.makeText(getContext(),""+message, Toast.LENGTH_SHORT).show();
    }

}
