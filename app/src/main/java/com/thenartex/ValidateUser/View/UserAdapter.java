package com.thenartex.ValidateUser.View;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.thenartex.ValidateUser.Model.User;
import com.thenartex.ValidateUser.R;
import java.util.ArrayList;
import java.util.List;

public class UserAdapter extends ArrayAdapter<User> {

    private Context mContext;
    private List<User> userList;

    public UserAdapter(Context context, ArrayList<User> list) {
        super(context,0, list);
        mContext = context;
        userList = list;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.list_item, parent, false);
        User currentUser = userList.get(position);

        TextView user = rowView.findViewById(R.id.tvUser);
        user.setText(currentUser.getEmail());

        TextView date =  rowView.findViewById(R.id.tvDate);
        date.setText(currentUser.getDate());

        TextView response = rowView.findViewById(R.id.tvResponse);
        if(currentUser.getResponse().equals("0"))
            response.setText("Access denied");
        else
            response.setText("Access allowed");
        return rowView;
    }
}
