package com.thenartex.ValidateUser;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.thenartex.ValidateUser.Model.User;
import java.util.ArrayList;
import java.util.List;


public class DataBaseHelper extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "UserData.db";

    // User table name
    private static final String TABLE_USER = "user";

    // Try table name
    private static final String TABLE_TRY = "try";

    // User Table Columns names
    private static final String COLUMN_USER_ID = "user_id";
    private static final String COLUMN_USER_EMAIL = "user_email";
    private static final String COLUMN_USER_PASSWORD = "user_password";

    // TRY Table Columns names
    private static final String COLUMN_TRY_ID = "try_id";
    private static final String COLUMN_TRY_EMAIL = "try_email";
    private static final String COLUMN_TRY_RESULT = "try_result";
    private static final String COLUMN_TRY_DATE = "try_date";

    // create USER table sql query
    private String CREATE_USER_TABLE = "CREATE TABLE " + TABLE_USER + "("
            + COLUMN_USER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COLUMN_USER_EMAIL + " TEXT," + COLUMN_USER_PASSWORD + " TEXT" + ")";

    // create TRY table sql query
    private String CREATE_TRY_TABLE = "CREATE TABLE " + TABLE_TRY + "("
            + COLUMN_TRY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COLUMN_TRY_EMAIL + " TEXT," + COLUMN_TRY_RESULT + " TEXT,"+ COLUMN_TRY_DATE + " TEXT" + ")";

    // drop table sql query
    private String DROP_USER_TABLE = "DROP TABLE IF EXISTS " + TABLE_USER;
    private String DROP_TRY_TABLE = "DROP TABLE IF EXISTS " + TABLE_TRY;

    /**
     * Constructor
     *
     * @param context
     */

    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }



    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_USER_TABLE);
        db.execSQL(CREATE_TRY_TABLE);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        //Drop User Table if exist
        db.execSQL(DROP_USER_TABLE);
        //Drop User Table if exist
        db.execSQL(DROP_TRY_TABLE);
        // Create tables again
        onCreate(db);

    }

    /**
     * This method is to create user record
     *
     * @param user
     */

    public boolean addUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_USER_EMAIL, user.getEmail());
        values.put(COLUMN_USER_PASSWORD, user.getPassword());

        // Inserting Row
        db.insert(TABLE_USER, null, values);
        db.close();

        return true;
    }

    /**
     * This method is to create try's record
     *
     * @param email
     * @param isValid
     */

    public void addTry(String email, boolean isValid, String currentDate) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_TRY_EMAIL, email);
        values.put(COLUMN_TRY_RESULT, isValid);
        values.put(COLUMN_TRY_DATE, currentDate);

        // Inserting Row
        db.insert(TABLE_TRY, null, values);
        db.close();
    }

    /**
     * This method is to fetch all user and return the list of user records
     *
     * @return list
     */
     public List<User> getAllUser() {
        // array of columns to fetch
        String[] columns = {
                COLUMN_TRY_ID,
                COLUMN_TRY_EMAIL,
                COLUMN_TRY_DATE,
                COLUMN_TRY_RESULT
        };
        // sorting orders
        String sortOrder =
                COLUMN_TRY_ID + " ASC";
        List<User> userList = new ArrayList<User>();

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_TRY, //Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                sortOrder); //The sort order


         if (cursor.moveToFirst()) {
             do {
                 User user = new User();
                 user.setEmail(cursor.getString(cursor.getColumnIndex(COLUMN_TRY_EMAIL)));
                 user.setDate(cursor.getString(cursor.getColumnIndex(COLUMN_TRY_DATE)));
                 user.setResponse(cursor.getString(cursor.getColumnIndex(COLUMN_TRY_RESULT)));
                 // Adding user record to list
                 userList.add(user);
             } while (cursor.moveToNext());
         }
         cursor.close();
         db.close();

         // return user list
         return userList;

    }


    /**
     * This method to check user exist or not
     *
     * @param email
     * @param password
     * @return true/false
     */
    public boolean checkUser(String email, String password) {

        // array of columns to fetch
        String[] columns = {
                COLUMN_USER_ID
        };
        SQLiteDatabase db = this.getReadableDatabase();
        // selection criteria
        String selection = COLUMN_USER_EMAIL + " = ?" + " AND " + COLUMN_USER_PASSWORD + " = ?";

        // selection arguments
        String[] selectionArgs = {email, password};

        Cursor cursor = db.query(TABLE_USER, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order

        int cursorCount = cursor.getCount();

        cursor.close();
        db.close();
        if (cursorCount > 0) {
            return true;
        }

        return false;
    }
}