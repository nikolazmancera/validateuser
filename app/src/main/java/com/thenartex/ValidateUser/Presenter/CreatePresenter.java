package com.thenartex.ValidateUser.Presenter;
import com.thenartex.ValidateUser.Model.User;
import com.thenartex.ValidateUser.View.ICreateView;

public class CreatePresenter  implements ICreatePresenter{

    ICreateView createView;
    public CreatePresenter(ICreateView createView) {
        this.createView = createView;
    }

    @Override
    public void onCreateUser(String email, String password) {
        User user = new User(email, password);
        boolean patternMatch = user.patternsMatch();
        if(patternMatch)
            createView.onCreateResult("OK");
        else
            createView.onCreateResult("Email or password error");
    }
}
