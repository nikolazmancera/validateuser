package com.thenartex.ValidateUser.Presenter;

public interface ICreatePresenter {
    void onCreateUser(String email, String password);
}
