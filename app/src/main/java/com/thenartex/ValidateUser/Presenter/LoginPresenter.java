package com.thenartex.ValidateUser.Presenter;
import com.thenartex.ValidateUser.Model.User;
import com.thenartex.ValidateUser.View.ILoginView;

public class LoginPresenter implements ILoginPresenter {

    private ILoginView loginView;

    public LoginPresenter(ILoginView loginView) {
        this.loginView = loginView;
    }

    @Override
    public void onLogin(String email, String password) {
        User user = new User(email, password);
        boolean patternMatch = user.patternsMatch();
        if(patternMatch) {
            loginView.onLoginResult("OK");
        }
        else
            loginView.onLoginResult("Email or password error");
    }
}
