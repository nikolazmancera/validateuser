package com.thenartex.ValidateUser.Presenter;

public interface ILoginPresenter {
    void onLogin(String email, String password);
}
