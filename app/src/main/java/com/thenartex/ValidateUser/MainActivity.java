package com.thenartex.ValidateUser;
import androidx.appcompat.app.AppCompatActivity;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.thenartex.ValidateUser.View.Create;
import com.thenartex.ValidateUser.View.Login;

public class MainActivity extends AppCompatActivity {

    TextView btLoginMenu;
    TextView btCreateMenu;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Login loginFragment = new Login();
        getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout,loginFragment).commit();
        btLoginMenu = findViewById(R.id.btnLogin);
        btCreateMenu = findViewById(R.id.btnCreate);
        btLoginMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Login loginFragment = new Login();
                getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout,loginFragment).commit();
                btLoginMenu.setBackground(getResources().getDrawable(R.drawable.button_background_selected));
                btCreateMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
            }
        });
        btCreateMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Create creteFragment = new Create();
                getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout,creteFragment).commit();
                btLoginMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                btCreateMenu.setBackground(getResources().getDrawable(R.drawable.button_background_selected));
            }
        });
    }
}
